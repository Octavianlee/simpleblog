from flask.ext.wtf import Form
from wtforms import TextField, validators, TextAreaField
from simpleblog.category.models import Category
from simpleblog.dbsettings import db_session

class CategoryCreateForm(Form):
	name = TextField('Name', [validators.required(),validators.length(min = 1, max = 20)])
	description = TextAreaField('Description')

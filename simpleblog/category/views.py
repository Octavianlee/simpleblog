from flask import Flask,Blueprint,render_template,redirect,url_for,request,session,flash

from simpleblog.category.models import Category
from simpleblog.account.models import User
from simpleblog.category.forms import CategoryCreateForm
from simpleblog.dbsettings import db_session

backend = Blueprint("category_backend",__name__,url_prefix="/admin",template_folder='templates')

@backend.route('/category',methods = ['GET', 'POST'])
def index():
	if "username" not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))

	categories = Category.query.filter(Category.cId != 1).all()
	return render_template('category/backend/index.html', categories = categories)


@backend.route('/category/new',methods = ['GET', 'POST'])
def new():
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	form = CategoryCreateForm()

	if form.validate_on_submit():
		newcat = Category()
		form.populate_obj(newcat)
		db_session.add(newcat)
		db_session.commit()
		return redirect(url_for("category_backend.index"))
	return render_template('category/backend/new.html', form = form)

@backend.route('/category/<int:id>')
def view(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	category = Category.query.filter_by(cId = id).first()
	if not category:
		return NotFound(404)
	return render_template('category/backend/view.html', category = category)

@backend.route('/category/<int:id>/edit',methods = ['GET', 'POST'])
def edit(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('category_backend.index'))

	category = Category.query.filter_by(cId = id).first()
	form = CategoryCreateForm(request.form, category)
	if form.validate_on_submit():
		form.populate_obj(category)
		db_session.commit()
		return redirect(url_for('category_backend.index'))
	return render_template('category/backend/edit.html', form=form)

@backend.route('/category/<int:id>/remove', methods=['GET','POST'])
def remove(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	if id == 1:
		return redirect(url_for('category_backend.index'))
	Category.query.filter_by(cId = id).delete()
	db_session.commit()
	return redirect(url_for('category_backend.index'))

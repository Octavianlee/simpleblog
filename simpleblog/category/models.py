from sqlalchemy import Column,Integer,String,Text
from simpleblog.dbsettings import Base

class Category(Base):
	__tablename__ = 'Category'
	cId = Column(Integer, primary_key = True)
	name = Column(String(20), unique = True)
	description = Column(Text)

	def __repr__(self):
		return '<Category %r>' % (self.name)

	def __unicode__(self):
		return self.name



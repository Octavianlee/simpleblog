from flask import Flask

#to start the whole project
app = Flask(__name__)
app.secret_key = '~\x9c)\xb0N\x9d\xd8\xcc\xb9A\xfb\xdf\xe5\x8b\x87\xd7\x1f\xde F\x9c.\xcaf'

from simpleblog import dbsettings
from simpleblog.account import *
from simpleblog.post import *
from simpleblog.category import *

from simpleblog.account.views.backend import backend
app.register_blueprint(backend)
from simpleblog.account.views.frontend import frontend
app.register_blueprint(frontend)
from simpleblog.post.views.frontend import frontend
app.register_blueprint(frontend)
from simpleblog.post.views.backend import backend
app.register_blueprint(backend)
from simpleblog.category.views import backend
app.register_blueprint(backend)
from simpleblog.comment.views.frontend import frontend
app.register_blueprint(frontend)
from simpleblog.comment.views.backend import backend
app.register_blueprint(backend)

from flask.ext.wtf import Form
from wtforms import TextField, validators, TextAreaField, HiddenField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from simpleblog.dbsettings import db_session
from simpleblog.comment.models import Comment

class CommentCreateForm(Form):
	content = TextAreaField('Comment:', [validators.Required("Please enter the content!")])



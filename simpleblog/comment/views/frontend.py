from flask import Flask,Blueprint,render_template,redirect,url_for,request,session,flash

from simpleblog.comment.models import Comment
from simpleblog.comment.forms import CommentCreateForm
from simpleblog.post.models import Post
from simpleblog.account.models import User
from simpleblog.dbsettings import db_session


frontend = Blueprint("comment_frontend",__name__,template_folder='../templates')

@frontend.route('/post/<int:id>/comment',methods=['GET', 'Post'])
def new(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.view', id = id))
	post = Post.query.filter_by(pId = id).first()
	if post == None:
		return redirect(url_for('post_frontend.index'))

	user = User.query.filter_by(userName = session['username']).first()
	authorId = user.uId
	postId = id
	newComment = Comment(request.args.get('content', ''))
	newComment.authorId = authorId
	newComment.postId = postId
	post.comments.append(newComment)
	db_session.add(newComment)
	db_session.commit()
	return redirect(url_for('post_frontend.view',id = post.pId))


@frontend.route('/post/<int:pId>/<int:commentId>/remove',methods=['GET', 'Post'])
def remove(pId,commentId):
	if 'username' not in session:
		return redirect(url_for('post_frontend.view', id = pId))
	post = Post.query.filter_by(pId = pId).first()
	comment = Comment.query.filter_by(id = commentId).first()
	user = User.query.filter_by(userName = session['username']).first()
	if post == None or comment == None:
		flash("wrong request")
		return redirect(url_for('post_frontend.index'))
	if comment.authorId == user.uId or user.uId == 1 or post.authorId == user.uId:
		Comment.findById(commentId).delete()
		db_session.commit()
	return redirect(url_for('post_frontend.view',id = post.pId))

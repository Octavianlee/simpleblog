from flask import Flask,Blueprint,render_template,redirect,url_for,request,session,flash

from simpleblog.comment.models import Comment
from simpleblog.comment.forms import CommentCreateForm
from simpleblog.post.models import Post
from simpleblog.account.models import User
from simpleblog.dbsettings import db_session

backend = Blueprint("comment_backend",__name__,url_prefix="/admin",template_folder='../templates')

@backend.route('/comment/', methods=['GET', 'POST'])
@backend.route('/comment/index',methods=['GET', 'Post'])
def index():
	if "username" not in session:
		return redirect(url_for('comment_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('comment_frontend.index'))

	comments = Comment.query.all()
	#if comments == None:
	#	abort(404)
	return render_template('comment/backend/index.html', comments = comments)

@backend.route('/comment/<int:id>/remove', methods=['GET','POST'])
def remove(id):
	if 'username' not in session:
		return redirect(url_for('comment_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('comment_frontend.index'))

	q_comment = Comment.findById(id)
	comment = q_comment.first()
	if comment == None:
		flash("wrong request")
		return redirect(url_for('comment_backend.index'))
	q_comment.delete()
	db_session.commit()
	return redirect(url_for('comment_backend.index'))

from sqlalchemy import Column,Integer,DateTime,Text,ForeignKey
from sqlalchemy.orm import relationship, backref
from datetime import datetime
from simpleblog.dbsettings import Base

class Comment(Base):
	__tablename__ = 'Comment'
	id = Column(Integer,primary_key = True)
	content = Column(Text)
	authorId = Column(Integer)
	postId = Column(Integer,ForeignKey('Post.pId'))
	publishTime = Column(DateTime, default = datetime.utcnow)
	commentId = Column(Integer,ForeignKey('Comment.id'))

	def __init__(self, content):
		self.content = content

	def __repr__(self):
		return '<Comment %r>' % (self.id)

	@classmethod
	def findById(cls, id):
		return Comment.query.filter(Comment.id == id)

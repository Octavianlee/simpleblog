from flask import Flask,render_template,session
from simpleblog import app
from simpleblog.models import *



@app.errorhandler(404)
def NotFound(e):
	return render_template('404.html'),404

from flask import Flask,Blueprint,render_template,redirect,url_for,request,session,flash
from simpleblog.post.models import Post,Tag
from simpleblog.post.forms import PostCreateForm,PostUpdateForm
from simpleblog.account.models import User
from simpleblog.comment.forms import CommentCreateForm
from simpleblog.dbsettings import db_session
from datetime import datetime

frontend = Blueprint("post_frontend",__name__,template_folder='../templates')

@frontend.route('/', methods=['GET', 'POST'])
@frontend.route('/index', methods=['GET', 'POST'])
def index():
	if 'username' in session:
		user = User.query.filter_by(userName = session['username']).first()
		if user.roleId == 1:
			session["isadmin"] = True
	posts = Post.query.all()
	return render_template('post/index.html', posts = posts)


@frontend.route('/new', methods = ['GET', 'Post'])
def new():
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	user = User.query.filter_by(userName = session['username']).first()
	if user.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	form = PostCreateForm()
	if form.validate_on_submit():
		tags = form.tags.data
		tag = []
		for newtag in tags.split(",") :
			if len(newtag) <= 20:	
				tag.append(Tag(newtag))
		newPost = Post(form.title.data, form.content.data)
		newPost.categories = form.category.data
		newPost.tags = tag
		newPost.authorId = user.uId
		db_session.add(newPost)
		db_session.commit()
		return redirect(url_for('post_frontend.index'))
	return render_template('post/new.html', form=form)

@frontend.route('/post/<int:id>',methods = ['GET', 'Post'])
def view(id):
	if 'username' in session:
		user = User.query.filter_by(userName = session['username']).first()
		session["userid"] = user.uId
		session["roleid"] = user.roleId
	post = Post.findById(id).first()
	form = CommentCreateForm()
	if not post:
		return NotFound(404)

	return render_template('post/view.html', post = post, form = form)

@frontend.route('/post/<int:id>/edit',methods = ['GET', 'POST'])
def edit(id):
	post = Post.findById(id).first()

	form = PostUpdateForm(request.form,post)
	if form.validate_on_submit():
		Tag.query.filter(Tag.postId==id).delete()
		tags = form.tags.data
		newlist = []

		for newtag in tags.split(",") :
			if len(newtag) <= 20:	
				a = Tag(newtag)
				a.postId = id;
				newlist.append(Tag(a))
		post.title = form.title.data
		post.content = form.content.data
		post.categories = form.category.data

		post.tags = newlist
		db_session.commit()
		return redirect(url_for('post_frontend.index'))


	return render_template('post/edit.html', form=form)

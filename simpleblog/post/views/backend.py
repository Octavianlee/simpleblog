from flask import Flask,Blueprint,render_template,redirect,url_for,request,session,flash
from simpleblog.post.models import Post,Tag
from simpleblog.account.models import User
from simpleblog.post.forms import PostCreateForm,PostUpdateForm
from simpleblog.dbsettings import db_session

backend = Blueprint("post_backend",__name__,url_prefix="/admin",template_folder='templates')

@backend.route('/', methods=['GET', 'POST'])
@backend.route('/index', methods=['GET', 'POST'])
@backend.route('/post', methods=['GET', 'POST'])
def index():
	if "username" not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))

	posts = Post.query.all()
	return render_template('post/backend/index.html', posts = posts)



@backend.route('/post/<int:id>/remove', methods=['GET','POST'])
def remove(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	Tag.query.filter(Tag.postId==id).delete()
	Post.findById(id).delete()
	db_session.commit()
	return redirect(url_for('post_backend.index'))


@backend.route('/post/new', methods = ['GET', 'Post'])
def new():
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	form = PostCreateForm()
	if form.validate_on_submit():
		tags = form.tags.data
		tag = []
		for newtag in tags.split(",") :
			if len(newtag) <= 20:	
				tag.append(Tag(newtag))
		newPost = Post(form.title.data, form.content.data)
		newPost.categories = form.category.data
		newPost.tags = tag
		newPost.authorId = admin.uId
		db_session.add(newPost)
		db_session.commit()
		return redirect(url_for('post_backend.index'))
	return render_template('post/backend/new.html', form=form)

@backend.route('/post/<int:id>')
def view(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	post = Post.findById(id).first()
	if not post:
		return NotFound(404)
	return render_template('post/backend/view.html', post = post)

@backend.route('/post/<int:id>/edit',methods = ['GET', 'POST'])
def edit(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))

	post = Post.findById(id).first()
	form = PostUpdateForm(request.form,post)
	if form.validate_on_submit():
		Tag.query.filter(Tag.postId==id).delete()
		tags = form.tags.data
		newlist = []

		for newtag in tags.split(",") :
			if len(newtag) <= 20:	
				a = Tag(newtag)
				a.postId = id;
				newlist.append(Tag(a))
		post.title = form.title.data
		post.content = form.content.data
		post.categories = form.category.data

		post.tags = newlist
		db_session.commit()
		return redirect(url_for('post_backend.index'))


	return render_template('post/backend/edit.html', form=form)



from flask.ext.wtf import Form
from wtforms import TextField, validators, TextAreaField, HiddenField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from simpleblog.post.models import Post
from simpleblog.dbsettings import db_session
from simpleblog.category.models import Category
class PostCreateForm(Form):
	title = TextField('Title', [validators.Required("Please enter a title!"),validators.length(min = 1, max = 100)])
	content = TextAreaField('Content', [validators.Required("Please enter the content!")])
	category = QuerySelectField('Category',query_factory = lambda: db_session.query(Category), get_pk=lambda a: a.cId, get_label=lambda a: a.name)
	tags = TextField ('Taglist(use "." to separate)')

class PostUpdateForm(PostCreateForm):
	id = HiddenField()


from sqlalchemy import Column,Integer,String,DateTime,Text,ForeignKey
from sqlalchemy.orm import relationship, backref
from datetime import datetime
from simpleblog.dbsettings import Base

class Tag(Base):
	__tablename__ = 'Tag'
	tId = Column(Integer, primary_key = True)
	name = Column(String(20))
	postId = Column(Integer,ForeignKey('Post.pId'))

	def __init__(self, name):
		self.name = name

	def __repr__(self):
		return self.name

	def __unicode__(self):
		return self.name


class Post(Base):
	__tablename__ = 'Post'
	pId = Column(Integer, primary_key = True)
	title = Column(String(100))
	publishTime = Column(DateTime, default = datetime.utcnow)
	modifiedTime = Column(DateTime, default = datetime.utcnow, onupdate=datetime.utcnow)
	content = Column(Text)
	authorId = Column(Integer,default = 1)
	categoryId = Column(Integer,ForeignKey('Category.cId'))
	categories = relationship('Category', backref = 'post')
	tags = relationship('Tag', backref = 'post',order_by = 'Tag.tId')
	comments = relationship('Comment', backref = 'post',order_by = 'Comment.publishTime')

	def __init__(self, title, content):
		self.title = title
		self.content = content

	def __repr__(self):
		return '<Post %r>' % (self.title)

	@classmethod
	def findById(cls, id):
		return Post.query.filter(Post.pId == id)


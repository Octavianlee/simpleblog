from sqlalchemy import Column,Integer,String,ForeignKey
from sqlalchemy.orm import relationship, backref
from simpleblog.dbsettings import Base

class Role(Base):
	__tablename__ = 'Role'
	rId = Column(Integer, primary_key = True)
	name = Column(String(20), unique = True)

	def __init__(self, name):
		self.name = name

	def __repr__(self):
		return '<Role %r>' % (self.name)



from sqlalchemy import Column,Integer,String,ForeignKey
from sqlalchemy.orm import relationship, backref
from simpleblog.dbsettings import Base
from .role import Role

class User(Base):
	__tablename__ = 'User'
	uId = Column(Integer, primary_key = True)
	userName = Column(String(20),unique = True)
	password = Column(String(20))
	roleId = Column(Integer,ForeignKey('Role.rId'))

	def __init__(self, username , password):
		self.userName = username
		self.password = password

	def __repr__(self):
		return '<User %r>' % (self.userName)

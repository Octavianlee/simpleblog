from flask.ext.wtf import Form
from wtforms import TextField, validators, PasswordField, TextAreaField, HiddenField
from simpleblog.account.models import User

class AddUserForm(Form):
	username = TextField("Username", [validators.Required("Please enter your username!")])
	password = TextField('Password',[validators.Required("Please enter your password!")])
class UserForm(Form):
	password = TextField('Password',[validators.Required("Please enter your password!")])
	id = HiddenField()
	



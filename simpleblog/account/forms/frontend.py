from flask.ext.wtf import Form
from wtforms import TextField, validators, PasswordField, TextAreaField, HiddenField
from simpleblog.account.models import User

class SignupForm(Form):
	username = TextField("Username", [validators.Required("Please enter your username!")])
	password = PasswordField('Password',[validators.Required("Please enter your password!")])

	def validate(self):
		if not Form.validate(self):
			return False
		user = User.query.filter_by(userName = self.username.data.lower()).first()
		if user:
			self.username.errors.append("That username is already taken!")
			return False
		else:
			return True

class LoginForm(Form):
	username = TextField("Username", [validators.Required("Please enter your username!")])
	password = PasswordField('Password',[validators.Required("Please enter your password!")])

	def validate(self):
		if not Form.validate(self):
			return False

		user = User.query.filter_by(userName = self.username.data.lower()).first()
		if user:
			if user.password == self.password.data:
				return True
			else:
				self.password.errors.append(user.password)
				return False
		else:
			self.username.errors.append("The user does not exist!")
			return False


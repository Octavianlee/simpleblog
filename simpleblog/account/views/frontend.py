from flask import Flask,Blueprint,render_template,redirect,url_for,request,session,flash

from simpleblog.account.models import User
from simpleblog.account.forms import SignupForm,LoginForm
from simpleblog.dbsettings import db_session

frontend = Blueprint("account_frontend",__name__,template_folder='../templates')

@frontend.route('/login',methods=['GET','POST'])
def login():
	form = LoginForm()
	if request.method == 'POST':
		if not form.validate():
			return render_template('account/login.html', form = form)
		else:
			session["username"] = form.username.data
			flash('You were logged in')
			return redirect(url_for("post_frontend.index"))

	elif request.method == 'GET':
			return render_template('account/login.html', form = form)

@frontend.route('/logout')
def logout():
	if 'username' not in session:
		return redirect(url_for('account_frontend.login'))
	session.pop('username',None)
	if 'isadmin' in session:
		session.pop('isadmin',None)
	flash('You were logged out')
	return redirect(url_for('post_frontend.index'))

@frontend.route('/signup',methods = ['GET', 'POST'])
def signup():
	form = SignupForm()
	if request.method == 'POST':
		if not form.validate():
			return render_template('account/signup.html', form = form)
		else:
			newuser = User(form.username.data,form.password.data)
			newuser.roleId = 2
			db_session.add(newuser)
			db_session.commit()
			session["username"] = form.username.data
			flash('You were logged in')
			return redirect(url_for("post_frontend.index"))

	elif request.method == 'GET':
			return render_template('account/signup.html', form = form)

from flask import Flask,Blueprint,render_template,redirect,url_for,request,session,flash

from simpleblog.account.models import User
from simpleblog.account.forms import AddUserForm,UserForm
from simpleblog.dbsettings import db_session

backend = Blueprint("account_backend",__name__,url_prefix="/admin",template_folder='../templates')
@backend.route('/account',methods = ['GET', 'POST'])
def index():
	if "username" not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))

	users = User.query.filter(User.uId != 1).all()
	return render_template('account/backend/index.html', users = users)


@backend.route('/account/new',methods = ['GET', 'POST'])
def new():
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	form = AddUserForm()
	if form.validate_on_submit():
		newuser = User(form.username.data,form.password.data)
		newuser.roleId = 2
		db_session.add(newuser)
		db_session.commit()
		return redirect(url_for("account_backend.index"))
	return render_template('account/backend/new.html', form = form)

@backend.route('/account/user/<int:id>')
def view(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	user = User.query.filter_by(uId = id).first()
	if not user:
		return NotFound(404)
	return render_template('account/backend/view.html', user = user)

@backend.route('/account/user/<int:id>/edit',methods = ['GET', 'POST'])
def edit(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))

	user = User.query.filter_by(uId = id).first()
	form = UserForm(request.form, user)
	if form.validate_on_submit():
		user.password = form.password.data
		db_session.commit()
		return redirect(url_for('account_backend.index'))
	return render_template('account/backend/edit.html', form=form)

@backend.route('/account/user/<int:id>/remove', methods=['GET','POST'])
def remove(id):
	if 'username' not in session:
		return redirect(url_for('post_frontend.index'))
	admin = User.query.filter_by(userName=session["username"]).first()
	if admin.roleId != 1:
		return redirect(url_for('post_frontend.index'))
	if id == 1:
		return redirect(url_for('account_backend.index'))
	User.query.filter_by(uId = id).delete()
	db_session.commit()
	return redirect(url_for('account_backend.index'))







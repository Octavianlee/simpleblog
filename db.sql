DROP TABLE IF EXISTS `User`;
DROP TABLE IF EXISTS `Role`;
DROP TABLE IF EXISTS `Comment`;
DROP TABLE IF EXISTS `Tag`;
DROP TABLE IF EXISTS `Post`;
DROP TABLE IF EXISTS `Category`;

CREATE TABLE `Role` (
  `rId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`rId`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `Role` VALUES (1,'admin');
INSERT INTO `Role` VALUES (2,'user');


CREATE TABLE `User` (
  `uId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  PRIMARY KEY (`uId`),
  UNIQUE KEY `userName` (`userName`),
  KEY `roleId` (`roleId`),
  CONSTRAINT `User_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `Role` (`rId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `User` VALUES (1,'admin','admin',1);



CREATE TABLE `Category` (
  `cId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`cId`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `Category` VALUES (1,'default','default');


CREATE TABLE `Post` (
  `pId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `publishTime` datetime DEFAULT NULL,
  `modifiedTime` datetime DEFAULT NULL,
  `content` text,
  `authorId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  PRIMARY KEY (`pId`),
  KEY `categoryId` (`categoryId`),
  CONSTRAINT `Post_ibfk_1` FOREIGN KEY (`categoryId`) REFERENCES `Category` (`cId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `Tag` (
  `tId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `postId` int(11) DEFAULT NULL,
  PRIMARY KEY (`tId`),
  KEY `postId` (`postId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `Comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `authorId` int(11) DEFAULT NULL,
  `publishTime` datetime DEFAULT NULL,
  `postId` int(11) DEFAULT NULL,
  `commentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `postId` (`postId`),
  KEY `commentId` (`commentId`),
  CONSTRAINT `Comment_ibfk_1` FOREIGN KEY (`postId`) REFERENCES `Post` (`pId`),
  CONSTRAINT `Comment_ibfk_2` FOREIGN KEY (`commentId`) REFERENCES `Comment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
